#!/usr/bin/env python
# queries which user the executable belongs to
from config import config as json, users as config_user

# executable to find in config
executable = input().split(' ')

# cycle through all of config
for config_user in config_users:
    # print user if user owns executable
    if (executable in (json[user])):
        print(user)
