#!/usr/bin/env python


from subprocess import PIPE, Popen


def run(c): return create_process(f'{c}').decode("utf-8").split()

def create_process(command):
    process = Popen( args=command
                , stdout=PIPE
                , shell=True
                )
    return process.communicate()[0]
