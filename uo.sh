#!/bin/sh

EXEC=$0

if [ $EXEC == "uo"] || [ $EXEC == "uo.sh"]; then
    UO_CONFIG="$XDG_CONFIG_HOME"/uo
    MADE_USERS=$(
        for USER in `ls /home`; do
            [ -e /home/"$USER"/.config/sh/defaults/ ] \
                && echo "$USER"
        done
    )

    # creating users
    UNMADE_USERS=`echo $MADE_USERS | python unmadeUsers.py`
    for UNMADE_USER in $UNMADE_USERS; do
        UNMADE_SH_CONFIG=/home/"$UNMADE_USER"/.config/sh

        adduser $UNMADE_USER
        mkdir -p "$UNMADE_SH_CONFIG"
        cp "$UO_CONFIG"/defaults "$UNMADE_SH_CONFIG"/
        echo "source defaults/*" >> "$UNMADE_SH_CONFIG"/rc.sh
    done

    # destroying users
    UNUSED_USERS=`echo $MADE_USERS | python unusedUsers.py`
    for UNUSED_USER in $UNUSED_USERS; do
        deluser "$UNUSED_USER"
        rm -rf /home/"$UNUSED_USER"
    done

else
    # TODO: TEST USER.PY
    UNMADE_USER=`echo $EXEC | python user.py`

    su -l $UNMADE_USER -c "sh -i -c $0"
fi
