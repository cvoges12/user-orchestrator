#!/usr/bin/env python
# unmade users in the configuration
from config import users as config_users

# even those not made by uo
all_users = input().split(' ')

# users made by uo
uo_users = []
for config_user in config_users:
    for existing_user in all_users:
        if (config_user == existing_user):
            uo_users.append(existing_user)

# start with all of the config users
unmade_users = config_users
# then remove all the existing ones
for unmade_user in unmade_users:
    for existing_user in all_users:
        if (unmade_user == existing_user):
            unmade_users.remove(existing_user)

for unmade_user in unmade_users:
    print(unmade_user)
