#!/usr/bin/env python
# provide all meaningful information from config
import json

# config file to parsable string
_file = open("config.json", "r")
string = _file.read()
_file.close()

# string of config parsed to json object
config = json.loads(string)

# users in config
users = []
for user in config:
    users.append(user)
