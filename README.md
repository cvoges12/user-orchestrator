# User Orchestrator

This is a proof-of-concept repository.

The idea was "if every process has its own user".


The code is largely unmaintained,
and there are no plans to continue further development


UIDs go up to 2^32 (4294967296): [source](https://www.kernel.org/doc/html/latest/admin-guide/highuid.html)

So the number of users won't be exhausted on a single system.
